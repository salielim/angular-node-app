// IIFE
(function() {

    // Create A Module
    var MyApp = angular.module("MyApp", []);

    // Create a Function / Function Constructor
    var MyCtrl = function() {
        var myCtrl = this;
        // Define a Model in the Controller
        //myCtrl.message = "hello world"; // use myCtrl instead of this to prevent reassignment
        //myCtrl.email = "fred@gmail.com";

        myCtrl.name = "";
        hideString = null;

        myCtrl.clearForm = function() {
            console.log("value of name: " + myCtrl.name);
            console.log("in clearForm function");
            myCtrl.name = "";
        }

        myCtrl.processForm = function() {
            console.log("processing form: " + myCtrl.name);
        }

        myCtrl.hide = function() {
            var stars = "**********"
            if(hideString) {
                myCtrl.name = hideString;
                hideString = null;
                return;
            }
            hideString = myCtrl.name;
            myCtrl.name = stars.substring(0, myCtrl.name.length);
        }
    };

    MyApp.controller("MyCtrl", MyCtrl);

})();